from tkinter import *
import random
import sys
import argparse

#0=Vide 1=Arbre 2=feu 3=Cendre 

color_blue = 'blue' #test
color_green= 'green' #arbre
color_grey = 'grey' #cendre
color_white = 'white' #vide
color_red = 'red' #feu

def remplissage():
    for irows in range(len(laforet)) :
        for arbre in range(len(laforet[irows])) :
            #random de 0 à 1 permettant de savoir si la donnée en [irows][icols] est un arbre ou non 
            if random.random() < affo :
                laforet[irows][arbre] = 1

def clic_callback(event):
    #recupération des coordonnées du click
    irows=event.x//ncell_size #absicce
    icols=event.y//ncell_size #ordonnée
    #si la case sélectionné est un arbre alors on change la couleur par du rouge qui correspond au feu 
    if laforet[icols][irows] == 1:
        canvas.itemconfigure(plateau[icols][irows],fill=color_red)
        laforet[icols][irows] = 2 #actualisation du tableau avec le 2

def reset_canvas():
    for irows in range(len(passage)):
        for icols in range(len(passage[irows])):
            passage[irows][icols] = 0

def btn_click() :
    burn = 2 #cette variable va servir à chercher un arbre en feu

    while any(burn in sublist for sublist in laforet) == True: 
        btn_burn.config(state=DISABLED)
        master.after(ntime,brulure(laforet,canvas,plateau))
        master.update()
    btn_burn.config(state=NORMAL)

def verif_regle(laforet, irows, icols):
    nbburned = 0 #nombre de voisin en feu
    retour = 1 #variable a retourner

    if regle1.get() == 1 :
        retour = 1 #variable par defaut pour l'embrasement
    
    if regle2.get() == 1 :
        if irows < nrows-1:
            #test au sud
            if laforet[irows+1][icols] == 2 :
                nbburned = nbburned+1 
                
        if irows > 0 :
            #test au nord
            if laforet[irows-1][icols] == 2 :
                nbburned = nbburned+1
                
        if icols < ncols-1 :
            #test à l'est
            if laforet[irows][icols+1] == 2 :
                nbburned = nbburned+1
                
        if icols > 0 :
            #test à l'ouest
            if laforet[irows][icols-1] == 2 :
                nbburned = nbburned+1
        
        retour = 1-(1/(nbburned+1)) #application de la formule
                        
    return retour   #retour de la variable

def brulure(laforet,canvas,plateau):
    #parcour du tableau
    for irows in range(len(laforet)):
        for icols in range(len(laforet[irows])):
            rdm = random.random()
            #test pour savoir s'il y a des cendres
            if (laforet[irows][icols] == 3 and passage[irows][icols] == 0) :
                canvas.itemconfigure(plateau[irows][icols],fill = color_white)
                #actualisation des tableaux
                laforet[irows][icols] == 0 #vide
                passage[irows][icols] == 1 #marquage du passage par 1

            #test pour savoir s'il y a des arbres en feu
            if (laforet[irows][icols] == 2 and passage[irows][icols] == 0) :
                #met le feu 
                if irows < nrows-1:
                    res = verif_regle(laforet,irows+1,icols)
                    if (laforet[irows+1][icols] == 1 and rdm > 1-res) :
                        canvas.itemconfigure(plateau[irows+1][icols],fill=color_red)
                        laforet[irows+1][icols] = 2
                        passage[irows+1][icols] = 1

                if irows > 0:
                    res = verif_regle(laforet,irows-1,icols)
                    if (laforet[irows-1][icols] == 1 and rdm > 1-res) :
                        canvas.itemconfigure(plateau[irows-1][icols],fill=color_red)
                        laforet[irows-1][icols] = 2
                        passage[irows-1][icols]=1

                if icols < ncols-1 :
                    res = verif_regle(laforet,irows,icols+1)
                    if (laforet[irows][icols+1] == 1 and rdm > 1-res) :
                        canvas.itemconfigure(plateau[irows][icols+1],fill=color_red)
                        laforet[irows][icols+1] = 2
                        passage[irows][icols+1]=1

                if icols > 0 :
                    res = verif_regle(laforet,irows,icols-1)
                    if (laforet[irows][icols-1] == 1 and rdm > 1-res) :
                        canvas.itemconfigure(plateau[irows][irows-1],fill=color_red)
                        laforet[irows][icols-1] = 2
                        passage[irows][icols-1] = 1

                canvas.itemconfigure(plateau[irows][icols],fill = color_grey)
                laforet[irows][icols] = 3

    reset_canvas()

if  __name__  == "__main__" : 

    #Liste des arguments
    parser = argparse.ArgumentParser(description="Speed king BURRNNN !!!")
    parser.add_argument("-rows", help="Nombre de ligne", type=int, default=50) #nb de ligne
    parser.add_argument("-cols", help="Nombre de colonne", type=int, default=50) #nb de colonne
    parser.add_argument("-time", help="duré de l'animation", type=int, default=500) #temps d'animation
    parser.add_argument("-cell_size", help="Taille d'une cellule", type=int, default=10) #taille de la cellule
    parser.add_argument("-afforestation", help="Pourcentage de boisement", type=int, default=90) #le taux de boisement d'une cellule
    args = parser.parse_args()
    
    nrows = args.rows
    ncols = args.cols
    ntime = args.time
    ncell_size = args.cell_size
    affo = args.afforestation
    affo = ((affo)/100)
    #Fenetre main
    master = Tk()
    master.title("SPEED KING !!") 

    #canvas principal contenant les rectangles avec une taille dynamique
    canvas = Canvas(master, height=ncell_size*nrows,width=ncell_size*ncols)
    canvas.pack(side=TOP)
    
    #passage initialisé avec des 0 qui va contenir la foret
    laforet = [[0 for icols in range(ncols)] for irows in range(nrows)]
    
    #initialisation d'un passage de passage qui servira pour plus tard
    #sera rempli uniquement de 0 ou 1
    passage = [[0] * ncols for _ in range(nrows)]

    #remplissage (ligne 14)
    remplissage()
    
    #création des rectangles avec les pararmètres par défaut ou données en entrés et on les stockent dans passage
    #canvas.rectangle(coox de départ, cooy de départ, coox de départ, cooy d'arrivé, couleur) 
    #Au début tout les rectangles sont blancs pour correspondre au vide
    #on multiplie la taille de la cellule par le numéro de la colonne pour obtenir les coordonnées de départ puis pour les coordonnées d'arrivé on utilise le numéro de ligne/colonne suivant
    plateau = [[canvas.create_rectangle(icols*ncell_size,irows*ncell_size,(icols+1)*ncell_size,(irows+1)*ncell_size,fill=color_white) for icols in range(ncols)] for irows in range(nrows)]

    #en cas de click gauche avec la souris sur un rectangle
    canvas.bind('<Button-1>',clic_callback) #click_callback (ligne 21)

    #une fois le rectangle séléctionné, on appuie sur le boutton pour lancer l'automate
    btn_burn = Button(master, text="BURRRRNNNNN !!!!", command=btn_click,padx=10,pady=10) #btn_click (ligne 33)
    btn_burn.pack(side=TOP)

    #Regle numéro 2 avec x% de chance de prendre feu à t+1 grace a la formule 1-(1/(k+1)) avec k le nombre de voisins
    regle2 = IntVar()
    Checkbutton_regle2 = Checkbutton(master, text="Règle n°2",padx=10,pady=10,variable=regle2,onvalue=1,offvalue=0)
    Checkbutton_regle2.pack(side=RIGHT)

    #Regle numéro 1 qui dit que la cellule prend feu à t+1 si au moins un voisin est en feu
    regle1 = IntVar()
    Checkbutton_regle1 = Checkbutton(master, text="Règle n°1",padx=10,pady=10,variable=regle1,onvalue=1,offvalue=0)
    Checkbutton_regle1.pack(side=RIGHT)

    #affichage des arbres au lancement
    for irows in range(len(laforet)):
        for icols in range(len(laforet[irows])):
            if laforet[irows][icols] == 1 :
                canvas.itemconfigure(plateau[irows][icols],fill=color_green)
    
    master.mainloop()